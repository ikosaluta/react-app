import {Row, Col, Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import DesignImage from '../images/BATANGAS1.jpg'
import Decor from '../images/decor.jpeg'
import VA3 from '../images/VA3.jpg'
import './Highlights.css'

export default function Highlights() {

	return(	
			<Row className="mt-3 mb-3 p-3">

				<Col xs={12} md={4}>
					<Card className="cardHighLight p-3 mb-2">
					<Card.Img className="images" variant="top" src={DesignImage} />			      
				      <Card.Body>
				        <Card.Title><strong>We Design</strong></Card.Title>
				        <Card.Text>
				          Luna Quarter Builders, Inc. takes pride in its design team manned with skilled architects and engineers. Talk to our professionals on your design needs. 
				        </Card.Text>
				      </Card.Body>
				      <Button className="button">Inquire now</Button>
				    </Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighLight p-3 mb-2">
					<Card.Img className="images"  variant="top" src={VA3} />				      
				      <Card.Body>
				        <Card.Title><strong>We Build</strong></Card.Title>
				        <Card.Text>
				          We are a one-stop design and build company with a commitment to efficiently deliver projects with the right quality through close supervision of project implementation.
				        </Card.Text>
				      </Card.Body>
				      <Button className="button">Inquire now</Button>
				    </Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighLight p-3 mb-2">
					<Card.Img className="images" variant="top" src={Decor} />				      
				      <Card.Body>
				        <Card.Title><strong>Interior Decor</strong></Card.Title>
				        <Card.Text>
				          We deliver interior design solutions alongside structural requirements. We carefully choose interior decor/s of high caliber according to project specifications and client preference.
				        </Card.Text>
				      </Card.Body>
				      <Button className="button" as={Link} to="/products">View Products</Button>
				    </Card>
				</Col>

			</Row>


		)
}