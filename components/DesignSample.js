import {Carousel} from 'react-bootstrap'
import BATANGAS1 from '../images/BATANGAS1.jpg'
import SONATA1 from '../images/SONATA1.jpg'
import MAGNOLIA1 from '../images/MAGNOLIA1.jpg'
import MAGNOLIA3 from '../images/MAGNOLIA3.jpg'
import MAGNOLIA4 from '../images/MAGNOLIA4.jpg'
import JUBILATION2 from '../images/JUBILATION2.jpg'
import VA4 from '../images/VA4.jpg'
import RUFINO1 from '../images/RUFINO1.jpg'
import './Carousel.css'

export default function DesignSample () {

	return (
			<div className="p-3">
			<div className="carousel row justify-content-center m-3 mb-5 p-3">

				{/*design samples---------------------------------------------------------*/}
				<div className="row justify-content-center col-lg-6">
					<h1 className="text-center p-3">Design Samples</h1>																			
						<Carousel variant="dark" className="col-md-10 justify-content-center mb-4">


					      <Carousel.Item>
					        <img
					          className="d-block w-100 h-100"
					          src={BATANGAS1}
					          alt="Batangas Project"
					        />
					        {/*<Carousel.Caption>
					          <h5>First slide label</h5>
					          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
					        </Carousel.Caption>*/}
					      </Carousel.Item>


					      <Carousel.Item>
					        <img
					          className="d-block w-100 h-100"
					          src={SONATA1}
					          alt="Second slide"
					        />
					        {/*<Carousel.Caption>
					          <h5>Second slide label</h5>
					          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					        </Carousel.Caption>*/}
					      </Carousel.Item>


					      <Carousel.Item>
					        <img
					          className="d-block w-100 h-100"
					          src={MAGNOLIA1}
					          alt="Third slide"
					        />
					        {/*<Carousel.Caption>
					          <h5>Third slide label</h5>
					          <p>
					            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
					          </p>
					        </Carousel.Caption>*/}
					      </Carousel.Item>


					    </Carousel>
			    </div>



			    {/*project samples---------------------------------------------------------*/}
				<div className="row justify-content-center col-lg-6">
					<h1 className="text-center p-3">Project Sample</h1>																			
						<Carousel variant="dark" className="col-md-10 justify-content-center mb-4">


					      <Carousel.Item>
					        <img
					          className="d-block w-100 h-100"
					          src={MAGNOLIA3}
					          alt="Batangas Project"
					        />
					        {/*<Carousel.Caption>
					          <h5>First slide label</h5>
					          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
					        </Carousel.Caption>*/}
					      </Carousel.Item>


					      <Carousel.Item>
					        <img
					          className="d-block w-100 h-100"
					          src={MAGNOLIA4}
					          alt="Second slide"
					        />
					        {/*<Carousel.Caption>
					          <h5>Second slide label</h5>
					          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					        </Carousel.Caption>*/}
					      </Carousel.Item>


					      <Carousel.Item>
					        <img
					          className="d-block w-100 h-100"
					          src={VA4}
					          alt="Third slide"
					        />
					        {/*<Carousel.Caption>
					          <h5>Third slide label</h5>
					          <p>
					            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
					          </p>
					        </Carousel.Caption>*/}
					      </Carousel.Item>

					      <Carousel.Item>
					        <img
					          className="d-block w-100 h-100"
					          src={JUBILATION2}
					          alt="Third slide"
					        />
					        {/*<Carousel.Caption>
					          <h5>Third slide label</h5>
					          <p>
					            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
					          </p>
					        </Carousel.Caption>*/}
					      </Carousel.Item>

					      <Carousel.Item>
					        <img
					          className="d-block w-100 h-100"
					          src={RUFINO1}
					          alt="Third slide"
					        />
					        {/*<Carousel.Caption>
					          <h5>Third slide label</h5>
					          <p>
					            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
					          </p>
					        </Carousel.Caption>*/}
					      </Carousel.Item>


					    </Carousel>
			    </div>
			<br/><br/>

			</div>
			</div>
		
		)
}