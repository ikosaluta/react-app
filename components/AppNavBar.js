// import Navbar from 'react-bootstrap/Navbar'
// import Nav from 'react-bootstrap/Nav'
// import Container from 'react-bootstrap/Container'
// import { Fragment,useState } from 'react'
import { Fragment,useContext, useState } from 'react'
import { Navbar, Nav, Container} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'
import LOGO from '../images/LOGO.png'
import './AppNavBar.css'

export default function AppNavBar() {

	const {user} = useContext(UserContext)

	const [click, setClick] = useState(false)

	const handleClick = () => setClick(!click)

	// state to store the user information stored in the log in page
	// const [user, setUser] = useState(localStorage.getItem("email"))
	/*	
		Syntax:
			localStorage.getItem("propertyName")
		getItem() method that returns value of a specified object item
	*/

	// console.log(user);

	return(

			<Navbar id="navbar" expand="md" className="p-1">
		      <Container>
		      	{/*logo nav------------------------------------------------------*/}
		      	{
		      		(user.id !== null) ?
		      			(user.isAdmin === true) ?
		      				<Navbar.Brand as={Link} to="/admin"> <img width="60px" height="60px" src={LOGO} alt="logo"></img> </Navbar.Brand>
		      			:
		      				<Navbar.Brand as={Link} to="/home"> <img width="60px" height="60px" src={LOGO} alt="logo"></img> </Navbar.Brand>
		      		:
		      			<Navbar.Brand as={Link} to="/"> <img width="60px" height="60px" src={LOGO} alt="logo"></img> </Navbar.Brand>
		      	}
		        <Navbar.Toggle aria-controls="basic-navbar-nav" onClick={handleClick}><i className = {click ? "fa-solid fa-folder-open" : "fa-solid fa-folder"}></i></Navbar.Toggle>
		        
		        <Navbar.Collapse id="basic-navbar-nav" className="border-0 justify-content-end">
		          <Nav className="nav ml-auto">

		      	{/*home nav--------------------------------------------------------*/}
		      		{
		      			(user.id !== null) ?
		      				(user.isAdmin === true) ?
		      					<Nav.Link className="navlink" as={Link} to="/admin">Home <i class="fa-solid fa-house-user"></i> </Nav.Link>
		      				:
		      					<Nav.Link className="navlink" as={Link} to="/home">Home <i class="fa-solid fa-house-user"></i> </Nav.Link>
		      			:
		      				<Nav.Link className="navlink" as={Link} to="/">Home <i class="fa-solid fa-house-user"></i></Nav.Link>
		      		}


		        {/*products nav-------------------------------------------------*/}
		            {
		            	(user.isAdmin === true) ?
		            		<Nav.Link as={Link} to="/adminProducts">Products</Nav.Link>
		            	:
		            		<Nav.Link as={Link} to="/products">Products</Nav.Link>
		            }
		            
		         	{/*login/logout/register------------------------------------------------*/}
		            {
		            	(user.id !== null ) ?
		            		(user.isAdmin !== true) ?

			            		<Fragment>
			            		<Nav.Link as={Link} to="/myOrder">My orders <i class="fa-solid fa-bag-shopping"></i></Nav.Link>
			            		<Nav.Link as={Link} to="/logout">Log out <i class="fa-solid fa-arrow-right-from-bracket"></i></Nav.Link>
			            		</Fragment>
			            	:
			            		<Nav.Link as={Link} to="/logout">Log out <i class="fa-solid fa-arrow-right-from-bracket"></i></Nav.Link>
		            	:
		            	<Fragment>
		            	<Nav.Link as={Link} to="/login">Log in</Nav.Link>

		            	<Nav.Link as={Link} to="/register">Register</Nav.Link>
		            	</Fragment>
		            }
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
		)
}