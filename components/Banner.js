import {Row, Col, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import './Banner.css'

export default function Banner () {

	return (

			<Row>
				{/*className is a property*/}
				<Col id="banner" className="banner p-5 mb-4 justify-content-end">

					<div className="banner-title d-inline-block p-3">
						<h1 >Luna Quarter</h1>
						<h2 >Builders Inc.</h2> 
					</div>
					<br/><br/>
					<h5 className="banner-title d-inline-block p-2">Dreams are meant to come true. We’ll make it happen for you.</h5> <br/>
					<p className="banner-title d-inline-block p-2">Inquire now and get a free consultation and quotation</p><br/>
					<Button id="button" className="pl-4 pr-4" as={Link} to="/login">Inquire Now!</Button>
				</Col>
			</Row>
		)
}