import {useState, useEffect, useContext} from 'react'
import {Button, Container} from 'react-bootstrap'
import {Link, Navigate, useNavigate} from 'react-router-dom'
import AdminProductCard from './AdminProductCard'
import UserContext from '../UserContext'

export default function AdminProducts () {

	const {user} = useContext(UserContext)

	const [adminProducts, setAdminProducts] = useState([])


	useEffect(()=>{

			fetch('https://floating-atoll-72456.herokuapp.com/api/products/all', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {

				setAdminProducts(data.map(adminProduct => {
					return (

							<AdminProductCard key={adminProduct.id} adminProductProp={adminProduct} />

						)
				}))
			})

		

	},[])


	return (

			(user.id !== null) ?
				(user.isAdmin == true) ?
					<>

						<h1 className="text-center m-4"><strong>Services</strong></h1>
							
						<Container className="justify-content-end ml-auto">
							<Button className="button" as={Link} to='/addProduct'>Add Product</Button>
						</Container>
						
						{adminProducts}
					</>
				:
					<Navigate to="*" />
			:
				<Navigate to="*" />
		)	
}