import {useContext} from 'react'
import {Row, Col, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'

export default function AdminBanner () {

	const {user} = useContext(UserContext)

	return(
			<Row>
				<Col>
					<h1 className="mt-5 mb-5 mr-5 pt-4">Welcome back, Admin {user.firstName}!</h1>
				</Col>
			</Row>

		)
}