import {useState, useEffect, useContext} from 'react'
import{Button, Container} from 'react-bootstrap'
import {Navigate, useNavigate, useParams, Link} from 'react-router-dom'
import AdminOrderListCard from './AdminOrderListCard'
import UserContext from '../UserContext'

export default function AdminOrderList () {

	const{user} = useContext(UserContext)

	const {productId} = useParams()
	// console.log(productId)

	const [adminOrderList, setAdminOrderList] = useState([])


	// para makuha data ng product--------------------------------
	useEffect(()=> {

		fetch(`https://floating-atoll-72456.herokuapp.com/api/products/${productId}/orderList`, {

			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			// console.log(data)

			setAdminOrderList(data.map(adminOrderList => {
				return (
						<AdminOrderListCard key={adminOrderList.userId} adminProductProp={adminOrderList} />
					)
			}))
		})
	},[])


	return (
			(user.id !== null) ?
				(user.isAdmin == true) ?
					<>
						<h1 className="text-center m-4">Product Order List</h1>

						<Container className="p-3 justify-content-end ml-auto">
							<Button className="button" as={Link} to='/adminProducts'>Go Back</Button>
						</Container>
													
						{adminOrderList}
					</>
					:
					<Navigate to="*" />
			:
				<Navigate to="*" />
		)		
}