import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Navigate, useNavigate, useParams, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function	EditProduct () {

	// for authentication------------------------------------
	const {user} = useContext(UserContext)

	// for redirection after adding a product------------------
	const navigate = useNavigate()

	// get productId sa link----------------------------------
	const { productId } = useParams ()
	// console.log(productId)
	

	// state hooks to store values of input fields-----------------------------
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [isActive, setIsActive] = useState("")

	// save button-------------------------------------------------
	/*const [isActive, setIsActive] = useState(false)

	useEffect(() => {

		if(name !== "" && description !== "" && price !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[name, description, price])*/

	// --------------------------------------------------
		useEffect(() => {

			fetch(`https://floating-atoll-72456.herokuapp.com/api/products/${productId}`)
			.then(res => res.json())
			.then(data => {

				// console.log(data)
				
				setName(data.name)
				setDescription(data.description)
				setPrice(data.price)
				setIsActive(data.isActive)
			})
		}, [])


	// edit product sa database ----------------------------------------------------
	function editProduct(e) {

		e.preventDefault()

		fetch(`https://floating-atoll-72456.herokuapp.com/api/products/${productId}/update`, {

			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price,
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Edit Successful",
					icon: "success",
					text: "ge ge ge"
				})

				navigate("/adminProducts")
			} else {
				Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Di mo kasalanan. Please try again"
						})
			}
		})

	}
		

	


	return (
			(user.id !== null) ?
				(user.isAdmin == true) ?
					<Form className="mt-3" onSubmit={(e) => editProduct(e)}>
						<h1 className="text-center">Edit a Product</h1>

					{/*Product Name input-----------------------------------------*/}
				      <Form.Group className="mb-3" controlId="productName">
				        <Form.Label>Product Name</Form.Label>
				        <Form.Control 
				        	type="text" 
				        	placeholder={setName}
				        	value={name}
				        	onChange={e => {
				        		setName(e.target.value)
				        	}}
				        	required />
				      </Form.Group>

				    {/*Description input-----------------------------------------*/}
				      <Form.Group className="mb-3" controlId="description">
				        <Form.Label>Description</Form.Label>
				        <Form.Control 
				        	as="textarea"
				        	rows="3" 
				        	placeholder={setDescription}
				        	value={description}
				        	onChange={e => {
				        		setDescription(e.target.value)
				        	}}
				        	required />
				      </Form.Group>

				  {/*Price input-----------------------------------------*/}
				      <Form.Group className="mb-3" controlId="price">
				        <Form.Label>Price</Form.Label>
				        <Form.Control 
				        	type="number" 
				        	placeholder={setPrice}
				        	value={price}
				        	onChange={e => {
				        		setPrice(e.target.value)
				        	}}
				        	required />
				      </Form.Group>

				   {/*isActive input-----------------------------------------*/}
				      <Form.Group className="mb-3" controlId="price">
				        <Form.Label>isActive</Form.Label>
				        <Form.Control 
				        	type="boolean" 
				        	placeholder={setIsActive}
				        	value={isActive}
				        	onChange={e => {
				        		setIsActive(e.target.value)
				        	}}
				        	required />
				      </Form.Group>

					

				      
		      		<Button type="submit" id="submitBtn" className="button m-1">
				        Save
				      </Button>
		      
		      		<Button variant="danger" type="submit" id="submitBtn" as={Link} to='/adminProducts' className="m-1">
				        Cancel
				      </Button>
				      
				    </Form>

				:
					<Navigate to="*" />
			:
				<Navigate to="*" />
				
		)
		
}