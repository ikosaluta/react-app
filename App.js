// import {Fragment} from 'react'
import {useState, useEffect} from 'react'
import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Routes, Route} from 'react-router-dom'
import AppNavBar from './components/AppNavBar'
// admin pages imports---------------------------------------
import AdminHome from './Admin/AdminHome'
import AdminProducts from './Admin/AdminProducts'
import AddProduct from './Admin/AddProduct'
import EditProduct from './Admin/EditProduct'
import AdminOrderList from './Admin/AdminOrderList'
// user pages imports--------------------------------------
import Home from './pages/Home'
import UserHome from './pages/UserHome'
import Products from './pages/Products'
import ProductView from './pages/ProductView'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import MyOrder from './pages/MyOrder'
import './App.css';
import {UserProvider} from './UserContext'


function App() {

  // State hook for the user state that's defined here for a global scope
  const [user, setUser] = useState ({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  // This function is for clearing local storage upon logout
  const unsetUser = () => {
    localStorage.clear()
  }

  
  // 
  useEffect(()=>{

    fetch('https://floating-atoll-72456.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== 'undefined') {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })

  },[])


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar/>
        <Container>
        <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/home" element={<UserHome/>}/>
          <Route path="/products" element={<Products/>}/>
          <Route path="/products/:productId" element={<ProductView/>}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="/logout" element={<Logout/>}/>
          <Route path="/register" element={<Register/>}/>
          <Route path="/myOrder" element={<MyOrder/>}/>
          <Route path= "*" element={<Error/>}/>
          // admin pages-----------------------
          <Route path="/admin" element={<AdminHome/>}/>
          <Route path="/adminProducts" element={<AdminProducts/>}/>
          <Route path="/addProduct" element={<AddProduct/>}/>
          <Route path="/editProduct/:productId" element={<EditProduct/>}/>
          <Route path="/adminOrderList/:productId" element={<AdminOrderList/>}/>

        </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
