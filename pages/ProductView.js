import {useState, useEffect, useContext} from 'react'
import {Container, Card, Button, Row, Col} from 'react-bootstrap'
import {useParams, useNavigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import './ProductView.css'

export default function ProductView() {

	// para may authorization before mag enroll. para logged in users lang ang makakapag enroll---------------------------------------------------
	const { user } = useContext(UserContext)

	// useNavigate allows us to gain access to method that will allow us to redirect a user to a different page after enrolling a course. "useHistory"- previous name ng useNavigate
	const navigate = useNavigate() //useHistory

	// "useParams" para makuha niya yung courseId sa link------------------------------------------
	const { productId } = useParams()

	// set muna ng initial values for name,des,andprice-------------------------------------------
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [quantity, setQuantity] = useState(1)

	// for quantity button--------------------------------------------
	
	function decrementCount () {

		if(quantity >= 2) {
			setQuantity(prevCount => prevCount-1)
		}	
	}

	function incrementCount () {
		setQuantity(prevCount => prevCount+1)
	}


	// para makapag enroll---------------------------------------
	const enroll = (productId) => {
		console.log(productId)
		
		fetch(`https://floating-atoll-72456.herokuapp.com/api/users/order`, {

			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity,
				productCost: price
			})
		})
		.then(res=>res.json())
		.then(data => {
			// console.log(data)

			if(data === true){
				Swal.fire({
					title: "Successfully Availed!",
					icon: 'success',
					text: "You have succcessfully availed this service!"
				})

				navigate("/products")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: 'error',
					text: "Please try again"
				})
			}
		})
	}

	// para reassign yung makukuhang data------------------------
	useEffect(()=>{
		console.log(productId)

		fetch(`https://floating-atoll-72456.herokuapp.com/api/products/${productId}`)
		.then(res=> res.json())
		.then(data => {

			// console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	},[productId])

	return (

			<Container className="mt-3">
				<Row className="justify-content-center mt-5">
					<Col className="col-md-8 col-lg-6">
						<Card id="card">
				            <Card.Body className="text-center">
				                <Card.Title className="product-title mb-4">{name}</Card.Title>
				                <Card.Subtitle className="product-title">Description:</Card.Subtitle>
				                <Card.Text>{description}</Card.Text>
				                <Card.Subtitle className="product-title">Price:</Card.Subtitle>
				                <Card.Text>PhP {price}</Card.Text>
				                <Button className="button-productView" onClick={decrementCount}>-</Button>
				                <span className="quantity-span p-2 pl-4 pr-4">{quantity}</span>
				                <Button className="button-productView" onClick={incrementCount}>+</Button>



				                {
				                	user.id !== null ?

				                	<Button variant="primary" className="button-productView m-3" onClick={() => enroll(productId)}>Add to Cart <i class="fa-solid fa-cart-arrow-down"></i> </Button>
				                	:

				                	<Link className="button-productView btn btn-danger m-3" to="/login">Log in to buy</Link>
				                }
				                
				                <Button  className="button-productView" as={Link} to="/products">Cancel </Button>
				      
				   			 </Card.Body>
						</Card>

					</Col>
				</Row>
			</Container>
		)
}