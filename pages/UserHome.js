import UserBanner from '../components/UserBanner'
import Highlights from '../components/Highlights'
import DesignSample from '../components/DesignSample'


export default function Home () {

	return (

			<>
				<UserBanner/>
				<Highlights/>
				<DesignSample/>
				
			</>
		)
}