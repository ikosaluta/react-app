import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import DesignSample from '../components/DesignSample'



export default function Home () {

	return (

			<>
				<Banner/>
				<Highlights/>
				<DesignSample/>
				
			</>
		)
}